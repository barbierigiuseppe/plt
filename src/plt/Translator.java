package plt;

public class Translator {
	public static final String NIL = "nil";
	private String phrase;
	
	public Translator(String inputPhrase) {
		 phrase = inputPhrase;
	}

	public String getPhrase() {
		// TODO Auto-generated method stub
		return phrase;
	}

	public String translate() {
		if(startsWithVowel()&&phrase.endsWith("y")) {
			return phrase.concat("nay");
		}
		else if(startsWithVowel() && endsWithVowel()) {
			return phrase.concat("yay");
		}
		else if (startsWithVowel()&&!endsWithVowel()) {
			return phrase.concat("ay");
		}
		
		return NIL;
	}
	
	private boolean startsWithVowel() {
		if(phrase.startsWith("a") || phrase.startsWith("e") 
				|| phrase.startsWith("i") || phrase.startsWith("o")
				|| phrase.startsWith("u")|| phrase.startsWith("A") 
				|| phrase.startsWith("E")|| phrase.startsWith("I")
				|| phrase.startsWith("O")|| phrase.startsWith("U")){
			return true;
		}
			else {
				return false;
			}
			
	}
	
	private boolean endsWithVowel() {
		if(phrase.endsWith("a") || phrase.endsWith("e") 
				|| phrase.endsWith("i") || phrase.endsWith("o")
				|| phrase.endsWith("u")|| phrase.endsWith("A")
				|| phrase.endsWith("E")|| phrase.endsWith("I")
				|| phrase.endsWith("O")|| phrase.endsWith("U")){
			return true;
		}
			else {
				return false;
			}
			
	}

	private boolean containsPunctuations() {
		if(phrase.contains(".")||phrase.contains(",")||phrase.contains(":")
				||phrase.contains(";")||phrase.contains("?")||phrase.contains("!")
				||phrase.contains("'")||phrase.contains("(")||phrase.contains(")")) {
			return true;
		}else {
			return false;
		}
	}
	
	
	
	
	public String startsWithSingleConsonant() {
		 if (!startsWithVowel() ||  phrase.startsWith("y")) {
			 
			String wordToAppend = "ay";
			char firstLetter = phrase.charAt(0); //get the first letter
			
			phrase = phrase.substring(1);// remove the first letter
			phrase = phrase+firstLetter+wordToAppend; //phrase + firstletter at the end + ay
			
		 }
		 return phrase;
	}
	
	
	
	public String startsWithMoreConsonant() {
		
		if(!startsWithVowel()) {
			String wordToAppend = "ay";
			
			phrase = phrase.replaceAll("(^[^aeiou]+)(.*)","$2$1");
			
			phrase = phrase+wordToAppend;
		}
		
		return phrase;
	}
	
	
	public String translatePhraseWithMoreWords() {
		if(phrase.contains(" ")||phrase.contains("-")) {
			phrase = phrase.replaceAll("(\\w)(\\w*)", "$2$1ay");
		 }
		
		return phrase;
	}
	
	public String translatePhraseWithPunctuations() { //I choose to leave this method alone becaus it can be applied to single words too
		if(containsPunctuations()) {
			phrase = phrase.replaceAll("(\\w)(\\w*)", "$2$1ay");
		}
		
		return phrase;
	}
	
	public boolean isUpperCase() {
		for(char letter : phrase.toCharArray()) {
			if(!Character.isUpperCase(letter)) {
				return false;
			}
		}
		return true;
	}
	
	public String translatePhraseUpperCase() {
		if(isUpperCase() && endsWithVowel()) {
			String wordToAppend = "YAY";
			phrase = phrase + wordToAppend;
		}
		

		return phrase;
	}
		   
	public String translatePhraseTitleCase() {
		
		char firstLetter = phrase.charAt(0);
		if(Character.isUpperCase(firstLetter)) {
			String wordToAppend = "ay";
			//
			phrase = phrase.substring(1);// remove the first letter
			String upperLetter = phrase.substring(0,1).toUpperCase();
			phrase = phrase.substring(1);
			phrase = upperLetter+phrase+Character.toLowerCase(firstLetter)+wordToAppend; //
			
		}
		System.out.println(phrase);
		return phrase;
	}
}		 
		
		
		
	
