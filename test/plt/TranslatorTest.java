package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputTranslator() {
		String inputPhrase = ("hello world");
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("hello world",translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(translator.NIL,translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("askay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithASingleConsonant() {
		String inputPhrase = "hello";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay",translator.startsWithSingleConsonant());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {
		String inputPhrase =  "known";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay",translator.startsWithMoreConsonant());
		
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsContainingWhiteSpaces() {
		String inputPhrase = "hello world";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway",translator.translatePhraseWithMoreWords());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsContainingDash() {
		String inputPhrase = "well-being";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay",translator.translatePhraseWithMoreWords());
	}
	
	@Test
	public void testTranslationPhraseContainingPunctuations() {
		String inputPhrase = "hello world!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!",translator.translatePhraseWithPunctuations());
		
		
		
	}
	
	@Test
	public void testTranslationPhraseWithUpperCase() {
		String inputPhrase = "APPLE";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("APPLEYAY",translator.translatePhraseUpperCase());
	}
	
	@Test
	public void testTranslationPhraseWithTitleCase() {
		String inputPhrase = "Hello";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("Ellohay",translator.translatePhraseTitleCase());
	}
	
}
